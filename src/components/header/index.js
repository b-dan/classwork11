import React, { useEffect, useState } from "react"
import recvest from "../../servic"
import navItem from './nav-item'
import './header.css'


function Header(){
    const[ data, setData]=useState(null);
    const [ btns, setBtns]= useState(null);


useEffect(()=>{
    recvest('https://swapi.dev/api/').then((rez)=>{
       setData(rez)
       setBtns(navItem(rez))

    })
},[])


    return(
        <>
        <nav>
            <ul className = 'nav-menu'>
                {Array.isArray(btns)? btns:null}
            </ul>
        </nav>
        </>
    )
}
export default Header