

function navItem(data = {}) {
    const item = []
    for (let elem in data) {
        item.push(<li key={elem}><button type="button" className="btn btn-outline-success">{elem}</button></li>)
    }
    return item

}

export default navItem