import React from "react";
import CardPipel from '../cardpiple/cardpiple'
import Planets from '../planets'
import Header from "../header/index"
import {BrowserRouter as Router, Routes, Route} from "react-router-dom"



function App (){
    return(
        <>
        <Router>
            <Routes>
                <Route path="/" element={<Header/>}></Route>
                <Route path="/people" element={<CardPipel/>}></Route>
                <Route path="/planets" element={<Planets/>}></Route>
                <Route path="/films"></Route>
                <Route path="/species"></Route>
                <Route path="/vehicles"></Route>
                <Route path="/starships"></Route>
            </Routes>
        </Router>
        
        
        </>
    )
}
export default App;