import React, { useEffect, useState } from "react";
import recvest from "../../servic";

const Planets = () => {
    const [data, setData] = useState(null);
    const [next, setNext] = useState('https://swapi.dev/api/planets/');

    useEffect(()=>{
        recvest('https://swapi.dev/api/planets/').then((planets)=>{
            setData(planets.results)
            setNext(planets.next)
        })
    },[])

    function nextpadge(){
        recvest(next).then((planets)=>{
            setData([...data, ...planets.results])
            setNext(planets.next)
        })
    }

    return(
        <>
        <div className ="card-badi">
            {Array.isArray(data)? data.map((elem ,index)=>{ return (
                 <article className = 'card' key = {index*2 + 'q'}>
                 <div className = 'card-img'>
                     <img src={`https://starwars-visualguide.com/assets/img/planets/${index +1}.jpg`} alt={'No picture, there is.'}></img>
                 </div>
                 <div className = 'card-info'>
                     <h2>{elem.name}</h2>
                     <p className ='pipel-info'>Name: {elem.name} </p>
                     <p className ='pipel-info'>Climat: {elem.climate || 'нет данных'} </p>
                     <p className ='pipel-info'>Terrain: {elem.terrain} </p>
                     <p className ='pipel-info'>Population: {elem.population}</p>
                     <p className ='pipel-info'>Gravity: {elem.gravity}</p>
                     <p className ='pipel-info'>Diameter: {elem.diameter} </p>
                     <p className ='pipel-info'>Orbital period: {elem.orbital_period} </p>
                     <p className ='pipel-info'>Rotation period: {elem.rotation_period}</p>
                 </div>
             </article>

            )}):null}
            <div className = 'div-button'><button className = 'btn' onClick = {nextpadge}>Показать ещё</button></div>
        </div>
        </>
    )
}

export default Planets