import React,{useEffect, useState } from "react";
import recvest from '../../servic';
import "./card.css"

function CardPipel (){
    const [data , setData] = useState(null);
    const [next, setNext] = useState('https://swapi.dev/api/people')

    // recvest( "https://swapi.dev/api/people").then((rez)=>{ 
    //     setData(rez)
    //     console.log(rez);
    // })
    useEffect(()=>{
        // const rev = fetch("https://swapi.dev/api/people")
        // rev.then((data)=>{
        //   return data.json()
        // }).then((data)=>{
        //     setData(data)
        // })
        recvest( "https://swapi.dev/api/people").then((rez)=>{ 
        setData(rez.results)
        setNext(rez.next)
    })
        
    },[])

    function nextpadge(){
        recvest(next).then((rez)=>{
            setData([...data, ...rez.results])
            setNext(rez.next)
            console.log(rez.next)
        })
    }
    
    return(
        <>
        <div className ="card-badi">
            <div className = 'div-button'><button className = 'btn' onClick = {nextpadge}>Показать ещё</button></div>
            {Array.isArray(data)? data.map((elem ,index)=>{ return (
                 <article className = 'card' key = {index*2 + 'q'}>
                 <div className = 'card-img'>
                     <img src={`https://starwars-visualguide.com/assets/img/characters/${index +1}.jpg`} alt={'No picture, there is.'}></img>
                 </div>
                 <div className = 'card-info'>
                     <h2>{elem.name}</h2>
                     <p className ='pipel-info'>Birth Year: {elem.birth_year} </p>
                     <p className ='pipel-info'>Species: {elem.species.length || 'нет данных'} </p>
                     <p className ='pipel-info'>Height: {elem.height} </p>
                     <p className ='pipel-info'>Mass: {elem.mass}</p>
                     <p className ='pipel-info'>Gender: {elem.gender}</p>
                     <p className ='pipel-info'>Hair Color: {elem.hair_color} </p>
                     <p className ='pipel-info'>Skin Color: {elem.skin_color} </p>
                     <p className ='pipel-info'>Homeworld: {elem.homeworld}</p>
                 </div>
             </article>

            )}):null}
        </div>
        </>
    )
}
export default CardPipel 